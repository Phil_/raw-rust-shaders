extern crate image;
mod lib;
mod mandel;
mod raytrace;

use image::ImageBuffer;
use lib::RenderScene;
use mandel::{Frame, MandelImage};
use raytrace::RayImage;

enum render_t {
    RayTrace,
    Mandel,
}

fn main() {
    let (width, height) = (1001, 1001);
    let t = render_t::RayTrace;

    let r_image: Box<dyn RenderScene> = match t {
        render_t::Mandel => {
            // rendering the mandelbrot set
            let frame = Frame::new(-1.5, 0.5, -1.0, 1.0);
            Box::new(MandelImage::new(width, height, frame))
        }
        render_t::RayTrace => {
            // rendering a simple sphere using raytracing
            Box::new(RayImage::new(width, height))
        }
    };

    let mut img = ImageBuffer::new(width, height);
    img.enumerate_pixels_mut().for_each(|(x, y, pixel)| {
        let (r, g, b) = r_image.render(x, y);
        *pixel = image::Rgb([r, g, b]);
    });

    img.save("test.png").unwrap();
}
