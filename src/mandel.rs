use crate::RenderScene;

pub struct MandelImage {
    frame: Frame,
    width: u32,
    height: u32,
    maxiter: u8,
}

struct Point {
    x: f32,
    y: f32,
}

pub struct Frame {
    ll: Point,
    ur: Point,
}

impl MandelImage {
    pub fn new(width: u32, height: u32, frame: Frame) -> Self {
        MandelImage {
            width,
            height,
            frame,
            maxiter: 255,
        }
    }
}

impl Point {
    pub fn new(x: f32, y: f32) -> Self {
        Point { x, y }
    }
}

impl Frame {
    pub fn new(x1: f32, x2: f32, y1: f32, y2: f32) -> Self {
        Frame {
            ll: Point::new(x1, y1),
            ur: Point::new(x2, y2),
        }
    }
}

impl RenderScene for MandelImage {
    fn render(&self, x: u32, y: u32) -> (u8, u8, u8) {
        let frame = &self.frame;
        let (width, height) = (self.width, self.height);
        let (r, g, b): (u8, u8, u8);
        let x_f = frame.ll.x + (x as f32 / width as f32) * (frame.ur.x - frame.ll.x);
        let y_f = frame.ll.y + (y as f32 / height as f32) * (frame.ur.y - frame.ll.y);
        let mut znew;
        let mut count: u8 = 0;
        let (mut zx, mut zy) = (0.0, 0.0);

        while zx * zx + zy * zy < 256.0 && count < self.maxiter {
            znew = zx * zx - zy * zy + x_f;
            zy = 2.0 * zx * zy + y_f;
            zx = znew;
            count += 1;
        }

        if count == self.maxiter {
            r = 0;
        } else {
            r = count;
        }
        g = zx as u8;
        b = zy as u8;

        (r, g, b)
    }
}
