pub trait RenderScene {
    fn render(&self, x: u32, y: u32) -> (u8, u8, u8);
}
