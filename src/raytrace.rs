use crate::RenderScene;

pub trait RayObject {
    fn dist_to(&self, p: &Vec3) -> f32;
}

struct Light {
    pos: Vec3,
}

impl Light {
    fn new(x: f32, y: f32, z: f32) -> Self {
        Light {
            pos: Vec3::new(x, y, z),
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Vec2 {
    x: f32,
    y: f32,
}

#[derive(Copy, Clone, Debug)]
pub struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

impl Vec2 {
    fn new(x: f32, y: f32) -> Self {
        Vec2 { x, y }
    }
}

impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Vec3 { x, y, z }
    }

    pub fn items(&self) -> [f32; 3] {
        [self.x, self.y, self.z]
    }

    pub fn len(&self) -> f32 {
        self.items().iter().map(|x| x * x).sum::<f32>().sqrt()
    }

    pub fn normalize(&self) -> Vec3 {
        let len = self.len().abs();
        Vec3::new(self.x / len, self.y / len, self.z / len)
    }

    pub fn add_times(&self, other: &Vec3, times: f32) -> Vec3 {
        Vec3::new(
            self.x + times * other.x,
            self.y + times * other.y,
            self.z + times * other.z,
        )
    }

    pub fn dot(&self, other: &Vec3) -> f32 {
        self.items()
            .iter()
            .zip(other.items().iter())
            .map(|(x, y)| x * y)
            .sum()
    }
}

struct Sphere {
    pos: Vec3,
    radius: f32,
    color: Vec3,
}

impl Sphere {
    pub fn new(pos: Vec3, radius: f32, color: Vec3) -> Self {
        Sphere { pos, radius, color }
    }
}

impl RayObject for Sphere {
    fn dist_to(&self, p: &Vec3) -> f32 {
        self.pos.add_times(&p, -1.).len() - self.radius
    }
}

pub struct RayImage {
    width: u32,
    height: u32,
    max_steps: u32,
    surface_dist: f32,
    max_dist: f32,

    // make this a scene-like struct
    objects: Vec<Sphere>,
    light: Light,
    shadow_intensity: f32,
    min_brightness: f32,
}

impl RayImage {
    pub fn new(width: u32, height: u32) -> Self {
        let sphere = Sphere::new(Vec3::new(0., 1., 6.), 1.0, Vec3::new(255., 0., 0.));
        let light = Light::new(0., 5., 6.);

        let objects = vec![sphere];
        RayImage {
            width,
            height,
            max_steps: 100,
            surface_dist: 0.01,
            max_dist: 100.,
            objects,
            light,
            shadow_intensity: 0.3,
            min_brightness: 0.01,
        }
    }

    fn get_dist(&self, p: &Vec3) -> f32 {
        let obj_dist = self
            .objects
            .iter()
            .map(|x| x.dist_to(&p))
            .min_by(|a, b| a.partial_cmp(b).expect("Tried to compare NaN"))
            .unwrap();

        let ground_dist = p.y;

        if obj_dist < ground_dist {
            obj_dist
        } else {
            ground_dist
        }
    }

    fn raymarch(&self, ro: &Vec3, rd: &Vec3) -> f32 {
        let mut d_o = 0.0;
        for _ in 0..self.max_steps {
            let p = ro.add_times(&rd, d_o);
            let d_s = self.get_dist(&p);
            d_o += d_s;
            if d_s < self.surface_dist || d_o > self.max_dist {
                break;
            }
        }
        d_o
    }

    /// returns float between 0 and 1
    fn get_light(&self, p: &Vec3) -> f32 {
        // vector from the specific point to the light source
        // TODO implement multiple lights, calculate the sum
        let l = self.light.pos.add_times(p, -1.);

        // normal vector on that specific point
        let n = self.get_normal(p);

        // then calc the dot product and return it as diffusion factor
        let mut dif = n.dot(&l.normalize());
        if dif < self.min_brightness {
            dif = self.min_brightness;
        } else {
            let new_start_pos = p.add_times(&n, self.surface_dist * 1.1);
            if self.raymarch(&new_start_pos, &l) < l.len() {
                dif *= self.shadow_intensity;
            }
        }
        dif
    }

    /// gets the normal vector for that specific point in space
    fn get_normal(&self, p: &Vec3) -> Vec3 {
        // get the distance to the scene
        let d = self.get_dist(p);

        // then get another few distances
        Vec3::new(
            d - self.get_dist(&p.add_times(&Vec3::new(0.01, 0.0, 0.0), -1.)),
            d - self.get_dist(&p.add_times(&Vec3::new(0.0, 0.01, 0.0), -1.)),
            d - self.get_dist(&p.add_times(&Vec3::new(0.0, 0.0, 0.01), -1.)),
        )
        .normalize()
    }
}

impl RenderScene for RayImage {
    fn render(&self, x: u32, y: u32) -> (u8, u8, u8) {
        // camera pos
        let ro = Vec3::new(0., 1., 0.);
        // view direction
        let vd = Vec3::new(0., 0., 1.);

        // resulting ray direction
        let uvx = (x as f32 / (self.width as f32 * 0.5)) - 1.0;
        // up and down were being mixed up
        let uvy = -1. * (y as f32 / (self.height as f32 * 0.5)) + 1.0;
        let rd = Vec3::new(uvx + vd.x, uvy + vd.y, vd.z).normalize();

        // walk that ray and get the
        // distance to next object (or max dist)
        let dist = self.raymarch(&ro, &rd);

        // then go to the intersection point and check how much light
        // will be reflected towards the camera
        let p = ro.add_times(&rd, dist);
        let dif = self.get_light(&p);

        // use that as the rendered light intensity
        // ranging from 0 to 255 (u8) to display a color on the screen
        let col = (255. * dif) as u8;
        (col, col, col)
    }
}
